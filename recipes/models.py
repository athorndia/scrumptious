from django.db import models
from django.conf import settings
# from datetime import timedelta


'''
The model allows us to save data in the database.
It describes the data that we want to have in our application.

Everytime we add a new model or make a change to a model, we need to
run a migration.

'''

# class Ingredient(models.Model):
#     amount = models.CharField(max_length=50, default='1')
#     food_item = models.CharField(max_length=200)
#     description = models.TextField()

# class Tag(models.Model):
#     title = models.CharField(max_length=200)
#     description = models.TextField()

# Get the name of the class used for the User
USER_MODEL = settings.AUTH_USER_MODEL


class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title

    # prep_time = models.DurationField(default=timedelta(minutes=30))
    # cook_time = models.DurationField(default=timedelta(minutes=30))
    # serving_size = models.PositiveIntegerField(default=2)
    # ingredients = models.ManyToManyField(Ingredient, related_name='recipes')
    # tags = models.ManyToManyField(Tag)
    # ratings = ***

    # def instructions(self):
#         return Instruction.objects.filter(recipe=self)

#######################################################################


class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe,
        related_name="steps",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["step_number"]


######################################################################

# class Instruction(models.Model):
#     recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
#     step_number = models.PositiveIntegerField()
#     step_description = models.TextField()

#     class Meta:
#         ordering = ['step_number']

#######################################################################

class Ingredient(models.Model):
    amount = models.CharField(max_length=100, default='1')
    food_item = models.CharField(max_length=100)

    recipe = models.ForeignKey(
        Recipe,
        related_name="ingredients",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["food_item"]

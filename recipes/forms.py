from django.forms import ModelForm
from recipes.models import Recipe
from django import forms
from django.contrib.auth import get_user_model


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
        ]

    author = forms.ModelChoiceField(queryset=get_user_model().objects.all())
